import json
import os

import boto3


# overall
SERVICE_NAME = 'sortinghat'
cwd = os.getcwd()
pos = cwd.find(SERVICE_NAME)
if pos > 0:
    PACKAGE_DIR = cwd[:cwd.find(SERVICE_NAME)] + SERVICE_NAME
else:
    PACKAGE_DIR = cwd + '/' + SERVICE_NAME
LOGS_PATH = PACKAGE_DIR + '/logs'


# AWS
AWS_REGION_NAME = "us-east-2"


# AWS secret manager
def get_secret(secret_name):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=AWS_REGION_NAME
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )

    secret = get_secret_value_response['SecretString']

    return secret


# download files from S3 if needed
BUCKET_NAME = 'gatherscope-sortinghat'
s3 = boto3.resource('s3')

dirnames = [
    PACKAGE_DIR+'/data',
    PACKAGE_DIR+'/models'
]
for dirname in dirnames:
    if not os.path.exists(dirname):
        os.mkdir(dirname)
filenames = [
    'data/recent_content_corpus.csv',
    'models/topic_cluster_classifier.pkl',
    'models/subtopic_cluster_classifier.pkl',
]
for filename in filenames:
    if not os.path.exists(PACKAGE_DIR+'/'+filename):
        s3.Bucket(BUCKET_NAME).download_file(filename,
                                             PACKAGE_DIR+'/'+filename)


# PostgreSQL Connection
PG_HOST = 'gatherscope-us.crdrxdtqjqpf.us-east-2.rds.amazonaws.com'
PG_DBNAME = 'gatherscope_milli'
PG_CREDS = json.loads(get_secret('postgresql/sortinghat/creds'))
PG_USER = PG_CREDS['username']
PG_PASSWORD = PG_CREDS['password']


# MongoDB Connection
MDB_CREDS = json.loads(get_secret('mongodb/sortinghat/creds'))
MDB_USERNAME = MDB_CREDS['username']
MDB_PASSWORD = MDB_CREDS['password']
MDB_HOST = (("mongodb+srv://%s:%s@gatherscope-us-gg0fy"
             + ".mongodb.net/test?retryWrites=true")
            % (MDB_USERNAME, MDB_PASSWORD))

# collection
DEV_MODE = False
MIN_TEXT_WORD_CT = 100
MAX_WORDS_TO_CONSIDER = 100
MAX_CHARS_TO_CONSIDER = 25000
STRIP_STRINGS = [
    ('Breaking News Emails Get breaking news alerts and special reports. The'
     + ' news and stories that matter, delivered weekday mornings.'),
    ('Get celebs updates directly to your inbox Subscribe Thank you for'
     + ' subscribing We have more newsletters Show me See our privacy notice'
     + ' Could not subscribe, try again later Invalid Email\n\n'),
    ('ES Football Newsletter Enter your email address Please enter an email'
     + ' address Email address is invalid Fill out this field Email address is'
     + ' invalid You already have an account. Please log in or register with'
     + ' your social account'),
    "'s", "’s", "‘s",
    # these must go last or risk breaking above
    '  ',
    "'", "’", "‘", ',', '.', '!', '?', ':', ';', '(', ')', '|', '-',
]
TITLE_DROP_STRINGS = [
    'call transcript'
]

# classification
# topics & associated feed keywords
TOPIC_CATEGORIZATIONS_AND_KEYWORDS = {
    'Politics, Policy & Economics': ['government', 'policy', 'politic'],
    'Finance & Business': ['companies', 'business', 'financ', 'markets',
                           'stocks'],
    'Technology, Science & Medicine': ['science', 'scientific', 'tech',
                                       'medicine'],
    'U.S. Sports': ['baseball', 'basketball', 'cbb', 'cfb', 'golf',
                    'hockey', 'mlb', 'nba', 'nfl', 'nhl'],
    'World Sports': ['cricket', 'rugby', 'soccer', 'tennis'],
    'Entertainment & Arts': ['arts', 'book', 'celebrit', 'entertainment',
                             'fashion', 'movies', 'music'],
    'Lifestyle & Health': ['life', 'living', 'relationship', 'sex',
                           'wellness'],
    'Travel & Food': ['food', 'travel'],
    'Gear & Gadgets': ['gear', 'gadget'],
}
# inclusion logic: union of top-30 countries by population and top-30 countries
# by GDP
REGION_CATEGORIZATIONS_AND_KEYWORDS = {
    'U.S. & Canada': ['canada', 'united states', 'u.s.'],
    'Latin America': ['americas', 'argentina', 'brazil', 'colombia',
                      'latam', 'latin america', 'mexico'],
    'Europe': ['austria', 'belgium', 'europe', 'france', 'germany', 'italy',
               'netherlands', 'norway', 'poland', 'russia', 'spain', 'sweden',
               'switzerland', 'united kingdom', 'u.k.'],
    'Middle East & Africa': ['africa', 'congo', 'ethiopia', 'egypt',
                             'emirates', 'iran', 'kenya', 'middle east',
                             'nigeria', 'saudi', 'tanzania', 'turkey',
                             'u.a.e.'],
    'Asia & ANZ': ['asia', 'austral', 'bangladesh', 'china', 'india',
                   'indonesia', 'japan', 'korea', 'myanmar', 'pakistan',
                   'philippines', 'taiwan', 'thailand', 'vietnam', 'zealand']
}
MIN_TOPIC_PROB = 0.5
MIN_REGION_PROB = 0.5


# clustering
TOPICS_TO_CLUSTER = [
    'Politics, Policy & Economics',
    'Finance & Business',
    'Technology, Science & Medicine',
    'U.S. Sports',
    'World Sports',
    'Entertainment & Arts',
    # 'Lifestyle & Health',
    # 'Travel & Food',
]
REGIONS_TO_CLUSTER = [
    'U.S. & Canada',
    'Latin America',
    'Europe',
    'Middle East & Africa',
    'Asia & ANZ',
]
TOPIC_CLUSTER_CLASSIFIER_PATH = (
    'models/topic_cluster_classifier.pkl'
)
TOPIC_CLUSTER_CLASSIFIER_FEATURES = [
    'title_dist',
    'text_dist',
    'text_nouns_dist',
    'text_adjs_dist',
    'title_dense_dist',
    'text_dense_dist'
]
TOPIC_MIN_P_SAME_CLUSTER = 0.5
MIN_ARTICLES_IN_TOPIC_CLUSTER = 25
SUBTOPIC_CLUSTER_CLASSIFIER_PATH = (
    'models/subtopic_cluster_classifier.pkl'
)
SUBTOPIC_CLUSTER_CLASSIFIER_FEATURES = [
    'title_dist',
    'text_dist',
    'text_nouns_dist',
    'text_adjs_dist',
    'title_dense_dist',
    'text_dense_dist'
]
SUBTOPIC_MIN_P_SAME_CLUSTER = 0.4
LIGHT_QUEUE_MAX_CLUSTER_SIZE = 10000
MIN_ARTICLES_IN_SUBTOPIC_CLUSTER = 3
