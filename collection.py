from datetime import datetime, timedelta

from dateutil.parser import parse as timeparse
import pandas as pd
import psycopg2
from pymongo import ASCENDING, MongoClient

from categorization import categorize_by_keywords
import configs

# MongoDB Config
MDB_CLIENT = MongoClient(configs.MDB_HOST)
MILLI_MDB = MDB_CLIENT.milli_mdb
PARSED_CONTENT = MILLI_MDB.parsed_content


def get_publisher_feeds(city_id=None):
    """Gets publisher feeds and categorizes each by region and topic.

    Keyword arguments:
    None

    Returns:
    pandas DataFrame -- publisher feeds; each row is a feed and columns are:
        ['publisher', 'subcategory', 'feed_url', 'local_categorization',
        'region_categorization', 'topic_categorization']
    """
    sql = """
        SELECT
        publisher,
        subcategory,
        url AS feed_url,
        'Local - '||city_id as local_categorization,
        quality_score

        FROM feeds
        JOIN publishers
            on publishers.name = feeds.publisher

    """
    if city_id:
        sql += ('WHERE city_id = %s' % (city_id))
    else:
        sql += 'WHERE city_id is null'

    pg_conn = psycopg2.connect(
        host=configs.PG_HOST,
        dbname=configs.PG_DBNAME,
        user=configs.PG_USER,
        password=configs.PG_PASSWORD
    )
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute(sql)
    feeds = pg_cursor.fetchall()
    pg_cursor.close()
    pg_conn.close()

    feeds = pd.DataFrame(
        feeds,
        columns=['publisher', 'subcategory', 'feed_url',
                 'local_categorization', 'quality_score']
    )

    feeds['region_categorization'] = feeds['subcategory'].apply(
        lambda x: categorize_by_keywords(
            x.lower(),
            configs.REGION_CATEGORIZATIONS_AND_KEYWORDS
        )
    )
    feeds['region_categorization'] = (
        feeds['local_categorization'].combine_first(
            feeds['region_categorization']
        )
    )
    feeds['topic_categorization'] = feeds['subcategory'].apply(
        lambda x: categorize_by_keywords(
            x.lower(),
            configs.TOPIC_CATEGORIZATIONS_AND_KEYWORDS
        )
    )

    return feeds


def get_recently_crawled_content(publisher_feeds,
                                 days_of_content=7):
    """Gets recently-crawled publisher content from MongoDB.

    Keyword arguments:
    days_of_content -- days of recent content to pull, from days_of_content
        days ago until now (integer, default: 7)

    Returns:
    pandas DataFrame -- rows are publisher articles and columns are
        ['_id', 'crawled_timestamp', 'title', 'feed_url', 'text',
         'meta_keywords']

    """
    start_timestamp = str(datetime.utcnow()
                          - timedelta(days=days_of_content))
    end_timestamp = str(datetime.utcnow()
                        - timedelta(minutes=1))

    recent_content_cursor = PARSED_CONTENT.find(
        {'crawled_timestamp': {'$gt': start_timestamp,
                               '$lt': end_timestamp},
         'title_language': 'en'}
    ).sort([('crawled_timestamp', ASCENDING)])

    to_concat = []
    include_record = []
    for doc in recent_content_cursor:
        feed_url = doc['feed_url']
        if feed_url not in publisher_feeds:
            include_record.append(False)
            continue

        text = doc['text']
        word_count = len(text.split())
        if word_count < configs.MIN_TEXT_WORD_CT:
            include_record.append(False)
            continue

        title = doc['title']
        if not include_title(title):
            include_record.append(False)
            continue

        if configs.DEV_MODE:
            record = pd.DataFrame({
                '_id': [doc['_id']],
                'crawled_timestamp': [timeparse(doc['crawled_timestamp'])],
                'feed_url': [feed_url],
                'title': [title],
                'text': [text],
                'meta_keywords': [' '.join(doc['meta_keywords'])]
            })
        else:  # don't title, text, meta
            record = pd.DataFrame({
                '_id': [doc['_id']],
                'crawled_timestamp': [timeparse(doc['crawled_timestamp'])],
                'feed_url': [feed_url]
            })
        to_concat.append(record)
        include_record.append(True)

    recent_content = pd.concat(to_concat)
    recent_content.reset_index(inplace=True)
    recent_content.drop(labels=['index'], axis='columns', inplace=True)

    return recent_content, recent_content_cursor, include_record


def include_title(title):
    """Return True if no reason to drop title"""
    drop_strings = configs.TITLE_DROP_STRINGS
    include = True
    for str in drop_strings:
        include = (include and str not in title.lower())
    return include


def clean(text):
    """Strips unwanted text from a string (determined in configs).

    Keyword arguments:
    txt -- from which to strip unwanted text (string)

    Returns:
    string -- cleaned txt
    """
    strip_terms = configs.STRIP_STRINGS
    for term in strip_terms:
        if term in text:
            text = text.replace(term, '')

    text = text[:configs.MAX_CHARS_TO_CONSIDER]
    
    return text
