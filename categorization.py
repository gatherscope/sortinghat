import logging
import os

import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split

import configs

# logging config
LOGS_PATH = configs.LOGS_PATH
if not os.path.exists(LOGS_PATH):
    os.mkdir(LOGS_PATH)
logging.basicConfig(
    filename=LOGS_PATH+'/performance.log',
    format='%(asctime)s %(message)s',
    level=logging.DEBUG
)


def categorize_by_keywords(text, categorizations_and_keywords):
    """Assign text to a category based on its containing a keyword.

    Maps text to categorization based on its containing a keyword. A *very*
    basic implementation, it returns only one categorization to describe text
    which is determined by the last-detected keyword. This works fine if
    categorizations are mutually exclusive but not well otherwise.

    Keyword arguments:
    text -- text to search for keywords (string)
    categorizations_and_keywords -- categorizations and associated keywords in
        the format {'categorization':['kw1', 'kw2', 'kw3, ...] (dictionary)

    Returns:
    string -- text's categorization
    """
    for categorization, keywords in categorizations_and_keywords.items():
        for keyword in keywords:
            if keyword in text:
                return categorization
    return None


def svm_extrapolate_categorization(df, feature_matrix, categorization_colname,
                                   verbose=False):
    """Takes a df where some records are categorized and predicts the rest.

    Given a df where some documents are categorized, trains an SVM classifier
        on the categorized subset and then makes predictions for the entire df.

    Keyword arguments:
    df -- dataset to be categorized (pandas DataFrame)
    feature_matrix -- vectorized features for categorization training/fitting
        (numpy array)
    categorization_colname -- name of dependent variable (string)

    Returns:
    pandas DataFrame -- df with [categorization_colname]_predicted and
        [categorization_colname]_probability columns added.

    """
    df_ = df.copy()
    feature_matrix_ = feature_matrix.copy()
    filt = (~df_[categorization_colname].isna())
    y = df_[filt][categorization_colname].copy()
    indexes = np.where(filt)
    X = feature_matrix_[indexes].copy()
    X_train, X_test, y_train, y_test = (
        train_test_split(X, y, test_size=0.1)
    )
    del y, X

    sgd = SGDClassifier(
        loss='modified_huber',
        max_iter=100,
        n_jobs=2,
        learning_rate='optimal',
        penalty='l2',
        alpha=0.0001
    )
    sgd.fit(X_train, y_train)

    pred = sgd.predict(X_test)
    accuracy = metrics.accuracy_score(y_test, pred)
    logstr = ('\n svm classifier performance')
    logstr += ('\n accuracy:   %0.3f' % accuracy)
    logstr += ('\n classification report: \n')
    logstr += str(metrics.classification_report(y_test, pred))
    logstr += ('\n confusion matrix: \n')
    logstr += str(metrics.confusion_matrix(y_test, pred))
    if verbose:
        print(logstr)
    logging.info(logstr)

    df_[categorization_colname+'_predicted'] = (
        sgd.predict(feature_matrix_)
    )
    df_[categorization_colname+'_probability'] = (
        sgd.predict_proba(feature_matrix_).max(axis=1)
    )

    return df_
