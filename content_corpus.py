from datetime import datetime, timezone
import logging
import os

import numpy as np
from redis import Redis
from rq import Queue
from scipy.sparse import hstack

from clustering import form_topic_clusters
from collection import clean, get_publisher_feeds, get_recently_crawled_content
import configs
from categorization import svm_extrapolate_categorization
from vectorization import vectorize, dense_vectorize, vocab_to_pos

# logging config
LOGS_PATH = configs.LOGS_PATH
if not os.path.exists(LOGS_PATH):
    os.mkdir(LOGS_PATH)
logging.basicConfig(
    filename=LOGS_PATH+'/performance.log',
    format='%(asctime)s %(message)s',
    level=logging.DEBUG
)

# Redis Config
REDIS_CONN = Redis()
REDIS_QUEUE_LIGHT = Queue('light', connection=REDIS_CONN)
REDIS_QUEUE_HEAVY = Queue('heavy', connection=REDIS_CONN)


class ContentCorpus(object):
    """Pulls articles then vectorizes, categorizes, and clusters them."""
    def __init__(self, city_id=None, verbose=False):
        self.city_id = city_id
        self.df = None
        self.cursor = None
        self.include_record = None
        self.days_of_recent_content = None
        self.publisher_feeds = None
        self.verbose = verbose
        self.titles_tfidf_matrix = None
        self.titles_tfidf_vocab = None
        self.titles_dense_matrix = None
        self.texts_tfidf_matrix = None
        self.texts_tfidf_vocab = None
        self.texts_tfidf_pos = None
        self.texts_dense_matrix = None
        self.texts_nouns_tfidf_matrix = None
        self.texts_adjs_tfidf_matrix = None
        self.meta_keywords_tfidf_matrix = None
        self.meta_keywords_tfidf_vocab = None
        self.master_tfidf_matrix = None
        self.master_tfidf_vocab = None

    def load_recent_content(self, days_of_recent_content, df=None):
        """Loads recent content either from MongoDB or locally.

        Keyword arguments:
        days_of_recent_content -- the number of days of recent content to pull
            ending with the time of initialization
        df -- dataframe of articles data; if supplied this is not queried from
            MDB (pandas DataFrame, default: None)

        Returns:
        None
        """
        self.publisher_feeds = get_publisher_feeds(self.city_id)

        if df is None:
            self.days_of_recent_content = days_of_recent_content

            df, cursor, include_record = get_recently_crawled_content(
                list(self.publisher_feeds['feed_url']),
                days_of_recent_content
            )
            df = df.merge(
                self.publisher_feeds[['feed_url',
                                      'region_categorization',
                                      'topic_categorization',
                                      'quality_score']],
                how='inner'
            )
            df.drop(labels=['feed_url'], axis='columns', inplace=True)
            df.sort_values(by='crawled_timestamp', inplace=True)  # restore
            df.reset_index(inplace=True)
            df.drop(labels=['index'], axis='columns', inplace=True)

            self.df = df
            self.cursor = cursor
            self.include_record = include_record
        else:
            self.days_of_recent_content = days_of_recent_content
            self.df = df

    def vectorize_content(self):
        """Forms tfidf vectors for content titles, texts and meta keywords."""
        if self.cursor is not None and not configs.DEV_MODE:
            self.cursor.rewind()
            titles = (clean(doc['title'])
                      for idx, doc in enumerate(self.cursor)
                      if self.include_record[idx])
            self.titles_tfidf_matrix, self.titles_tfidf_vocab = (
                vectorize(titles, tfidf=True)
            )

            self.cursor.rewind()
            titles = (clean(doc['title'])
                      for idx, doc in enumerate(self.cursor)
                      if self.include_record[idx])
            self.titles_dense_matrix = dense_vectorize(titles)

            self.cursor.rewind()
            texts = (clean(doc['text'])
                     for idx, doc in enumerate(self.cursor)
                     if self.include_record[idx])
            self.texts_tfidf_matrix, self.texts_tfidf_vocab = (
                vectorize(texts, tfidf=True)
            )

            self.cursor.rewind()
            texts = (clean(doc['text'])
                     for idx, doc in enumerate(self.cursor)
                     if self.include_record[idx])
            self.texts_dense_matrix = dense_vectorize(texts)

            self.cursor.rewind()
            meta_keywords = (' '.join(doc['meta_keywords'])
                             for idx, doc in enumerate(self.cursor)
                             if self.include_record[idx])
            self.meta_keywords_tfidf_matrix, self.meta_keywords_tfidf_vocab = (
                vectorize(meta_keywords, tfidf=True)
            )
        else:
            titles = list(self.df['title'].apply(clean))
            self.titles_tfidf_matrix, self.titles_tfidf_vocab = (
                vectorize(titles, tfidf=True)
            )
            self.titles_dense_matrix = dense_vectorize(titles)

            texts = list(self.df['text'].apply(clean))
            self.texts_tfidf_matrix, self.texts_tfidf_vocab = (
                vectorize(texts, tfidf=True)
            )
            self.texts_dense_matrix = dense_vectorize(texts)

            self.df['meta_keywords'] = self.df['meta_keywords'].apply(
                lambda x: x if isinstance(x, str) else ''
            )
            meta_keywords = list(self.df['meta_keywords'])
            self.meta_keywords_tfidf_matrix, self.meta_keywords_tfidf_vocab = (
                vectorize(meta_keywords, tfidf=True)
            )

        self.master_tfidf_matrix = hstack(
            [self.titles_tfidf_matrix,
             self.texts_tfidf_matrix,
             self.meta_keywords_tfidf_matrix]
        ).tocsr()
        self.master_tfidf_vocab = (
            self.titles_tfidf_vocab
            + self.texts_tfidf_vocab
            + self.meta_keywords_tfidf_vocab
        )

        texts_tfidf_pos = vocab_to_pos(
            self.texts_tfidf_vocab
        )
        self.texts_tfidf_pos = np.array(texts_tfidf_pos)

        texts_nouns_idxs = np.where(
            [pos in ['NOUN', 'PROPN'] for pos in self.texts_tfidf_pos]
        )[0]
        self.texts_nouns_tfidf_matrix = self.texts_tfidf_matrix[
            :, texts_nouns_idxs
        ]

        texts_adjs_idxs = np.where(self.texts_tfidf_pos == 'ADJ')[0]
        self.texts_adjs_tfidf_matrix = self.texts_tfidf_matrix[
            :, texts_adjs_idxs
        ]

    def categorize_content(self):
        """Extrapolates feed-based topic and region categorizations."""
        if self.city_id:
            self.df['region_categorization_predicted'] = (
                self.df['region_categorization']
            )
            self.df['region_categorization_probability'] = (
                np.ones(len(self.df))
            )
            self.df['topic_categorization_predicted'] = (
                np.repeat('All', len(self.df))
            )
            self.df['topic_categorization_probability'] = (
                np.ones(len(self.df))
            )
        else:
            self.df = svm_extrapolate_categorization(
                self.df,
                self.master_tfidf_matrix,
                categorization_colname='topic_categorization',
                verbose=self.verbose
            )
            self.df = svm_extrapolate_categorization(
                self.df,
                self.master_tfidf_matrix,
                categorization_colname='region_categorization',
                verbose=self.verbose
            )

    def cluster_content(self, enqueue=True):
        """Assigns content to hierarchical clusters and writes to MongoDB."""
        if self.city_id:
            n_articles = len(self.df)
            matrices = {
                'titles_tfidf_matrix': self.titles_tfidf_matrix,
                'texts_tfidf_matrix': self.texts_tfidf_matrix,
                'texts_nouns_tfidf_matrix': self.texts_nouns_tfidf_matrix,
                'texts_adjs_tfidf_matrix': self.texts_adjs_tfidf_matrix,
                'titles_dense_matrix': self.titles_dense_matrix,
                'texts_dense_matrix': self.texts_dense_matrix
            }

            if n_articles >= configs.MIN_ARTICLES_IN_TOPIC_CLUSTER and enqueue:
                batch_timestamp = (
                    datetime.utcnow().replace(tzinfo=timezone.utc)
                )
                # Can add more logic here if more batch types introduced
                batch_type = (
                    'recent %sd' % str(self.days_of_recent_content)
                )
                if n_articles <= configs.LIGHT_QUEUE_MAX_CLUSTER_SIZE:
                    REDIS_QUEUE_LIGHT.enqueue(
                        form_topic_clusters, self.df, matrices,
                        batch_timestamp, batch_type,
                        False,  # return df
                        True,  # call form subtopic clusters
                        self.verbose,
                        timeout=21600, ttl=-1
                    )
                else:
                    REDIS_QUEUE_HEAVY.enqueue(
                        form_topic_clusters, self.df, matrices,
                        batch_timestamp, batch_type,
                        False,  # return df
                        True,  # call form subtopic clusters
                        self.verbose,
                        timeout=21600, ttl=-1
                    )
            elif enqueue:
                topic = self.df['topic_categorization_predicted'][0]
                region = self.df['region_categorization_predicted'][0]
                logging.warn(
                    """Only %d articles in topic %s and regions %s; not
                    enough for clustering""", n_articles, topic,
                    str(region)
                )
        else:
            for topic in configs.TOPICS_TO_CLUSTER:
                for region in configs.REGIONS_TO_CLUSTER:
                    filt = (
                        (self.df['topic_categorization_predicted']
                         == topic)
                        & (self.df['topic_categorization_probability']
                           >= configs.MIN_TOPIC_PROB)
                        & (self.df['region_categorization_predicted']
                           == region)
                        & (self.df['region_categorization_probability']
                           >= configs.MIN_REGION_PROB)
                    )
                    df_subset = self.df[filt]
                    n_articles = len(df_subset)

                    indices = df_subset.index.values
                    matrices = {
                        'titles_tfidf_matrix':
                            self.titles_tfidf_matrix[indices],
                        'texts_tfidf_matrix':
                            self.texts_tfidf_matrix[indices],
                        'texts_nouns_tfidf_matrix':
                            self.texts_nouns_tfidf_matrix[indices],
                        'texts_adjs_tfidf_matrix':
                            self.texts_adjs_tfidf_matrix[indices],
                        'titles_dense_matrix':
                            self.titles_dense_matrix[indices],
                        'texts_dense_matrix':
                            self.texts_dense_matrix[indices]
                    }

                    df_subset = df_subset.reset_index()

                    if (n_articles >= configs.MIN_ARTICLES_IN_TOPIC_CLUSTER
                       and enqueue):
                        batch_timestamp = (
                            datetime.utcnow().replace(tzinfo=timezone.utc)
                        )
                        # Can add more logic here if more batch types added
                        batch_type = (
                            'recent %sd' % str(self.days_of_recent_content)
                        )
                        if n_articles <= configs.LIGHT_QUEUE_MAX_CLUSTER_SIZE:
                            REDIS_QUEUE_LIGHT.enqueue(
                                form_topic_clusters, df_subset, matrices,
                                batch_timestamp, batch_type,
                                False,  # return df
                                True,  # call form subtopic clusters
                                self.verbose,
                                timeout=21600, ttl=-1
                            )
                        else:
                            REDIS_QUEUE_HEAVY.enqueue(
                                form_topic_clusters, df_subset, matrices,
                                batch_timestamp, batch_type,
                                False,  # return df
                                True,  # call form subtopic clusters
                                self.verbose,
                                timeout=21600, ttl=-1
                            )
                    elif enqueue:
                        logging.warn(
                            """Only %d articles in topic %s and regions %s; not
                            enough for clustering""", n_articles, topic,
                            str(region)
                        )


def enqueue_content_for_clustering(days_of_recent_content=2, city_id=None):
    """Runs clustering procedures (schedule with cron).

    Keyword arguments:
    days_of_recent_content -- the number of days of recent content to pull
        ending with the time of initialization (integer, default: 3)

    Returns:
    None, but creates a content_corpus object and ultimately enqueues into rq
        a form_topic_clusters job
    """
    current_timestamp = datetime.utcnow().replace(tzinfo=timezone.utc)
    outstr = (
        'Clustering begun at: %s - %sd' %
        (str(current_timestamp), str(days_of_recent_content))
    )
    print(outstr)
    logging.info(outstr)
    recent_content_corpus = ContentCorpus(
        city_id=city_id,
        verbose=False
    )
    recent_content_corpus.load_recent_content(
        days_of_recent_content=days_of_recent_content
    )
    recent_content_corpus.vectorize_content()
    recent_content_corpus.categorize_content()
    recent_content_corpus.cluster_content()
    current_timestamp = datetime.utcnow().replace(tzinfo=timezone.utc)
