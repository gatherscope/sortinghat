import numpy as np
from scipy.cluster.hierarchy import linkage as agglomerative_cluster
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import spacy
from spacy.tokens import Doc

import configs

NLP = spacy.load('en_core_web_lg')
# fix spacy stop words bug associated with loading vocab
NLP.vocab.add_flag(
    lambda s: s.lower() in spacy.lang.en.stop_words.STOP_WORDS,
    spacy.attrs.IS_STOP
)


class WhitespaceTokenizer(object):
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, text):
        words = text.split(' ')
        # All tokens 'own' a subsequent space character in this tokenizer
        spaces = [True] * len(words)
        return Doc(self.vocab, words=words, spaces=spaces)


def lemmatize(text, pos_to_keep=['NOUN', 'PROPN', 'VERB', 'ADJ', 'ADV'],
              override_pos_filter=False, single_str_output=True,
              incl_pos=False, return_vectors=False):
    """Decomposes text into lemma tokens.

    Keyword arguments:
    text -- text to decompose  (string)
    pos_to_keep -- spacy-abbreviated part-of-speech types; a word's lemma will
        only appear in output if it is one of these types (list of strings,
        default: ['NOUN', 'PROPN', 'VERB', 'ADJ', 'ADV'])
    override_pos_filter -- if True, pos_to_keep is ignored and all parts of
        speech are kept (bool, default: False)
    single_str_output -- if True, output lemmas as a single string; if False,
        output lemmas as a list of strings (bool, default: True)
    include_pos -- whether to concanate a pos string suffix onto each token's
        lemma string (bool, default: False)
    return_vectors -- whether to additionally return a list of token embeddings
        (bool, default: False)

    Returns:
    list of strings or string (if single_str_output) -- text in lemmatized form
    list of arrays (if return_vectors) -- text in vector form

    """
    doc = NLP(text, disable=['ner', 'parser'])
    lemmas = []
    vectors = []
    for token in doc:
        if ((not token.is_stop)
                and (not (return_vectors and not token.has_vector))
                and (token.pos_ in pos_to_keep
                     or override_pos_filter)):
            lem = (token.lemma_
                   if not token.lemma_ == '-PRON-'
                   else token.string)
            if incl_pos:
                lemmas += [lem+'_'+token.pos_]
            else:
                lemmas += [lem]
            if return_vectors:
                vectors += [token.vector]
    if single_str_output:
        lemmas = ' '.join(lemmas)
    if return_vectors:
        return lemmas, vectors
    return lemmas


def simple_split(txt):
    """Splits txt by spaces (' ').

    Keyword arguments:
    txt -- to be split (string)

    Returns:
    list of words
    """
    return txt.split(' ')


def vectorize(docs, lemmatize_docs=False, max_synonym_merge_dist=None,
              tfidf=True):
    """Given some docs, transforms them into bag-of-words vector form.

    Given some docs, transforms them into bag-of-words vector form (with
        optional lemmatization and synonym merging in pre-processing
        and TF-IDF conversion in post).

    Keyword arguments:
    docs -- docs to transform (list of strings)
    lemmatize_docs -- whether to transform words to lemmas before vectorizing
        docs (bool, default: False)
    max_synonym_merge_dist -- max cosine distance between lemma glove
        embeddings which will be merged as identical (float, optional)
    tfidf -- whether to transform count bag-of-words vectorization into tfidf
        (bool, default: True)

    Returns:
    sparse array -- docs in vectorized form
    list of strings -- the tokens vocabulary corresponding to the docs and
        vectorization
    """
    # get initial vocab, vectors, embeddings
    if lemmatize_docs:
        lemmas = []
        lemma_2_vector = {}
        # in case a doc has only stop / not-embedded words
        # zeros break cosine agglomerative_cluster
        lemma_2_vector[''] = np.full((300,), 0.0001, dtype='float32')
        for doc in docs:
            doc_lemmas, doc_vectors = lemmatize(doc, single_str_output=False,
                                                return_vectors=True)
            for lemma, vector in zip(doc_lemmas, doc_vectors):
                lemma_2_vector[lemma] = vector
            lemmas += [' '.join(doc_lemmas)]

        count_vectorizer = CountVectorizer(
            ngram_range=(1, 1), min_df=2, max_df=1.0, lowercase=True,
            tokenizer=simple_split
        )
        doc_vectors_count = count_vectorizer.fit_transform(lemmas)
        idx_2_token = count_vectorizer.get_feature_names()
        lemma_vectors = np.array(
            [lemma_2_vector[token] for token in idx_2_token]
        )

        # collapse synonyms
        if max_synonym_merge_dist is not None:
            from clustering import gen_clusters  # avoid circular reference

            ac_result = agglomerative_cluster(
                lemma_vectors,
                method='average',
                metric='cosine',
                optimal_ordering=False
            )
            _, tokens_cluster_members = gen_clusters(
                ac_result,
                max_dist=max_synonym_merge_dist,
                return_cluster_members=True
            )
            tokens_to_delete = []
            for members in tokens_cluster_members.values():
                if len(members) > 1:
                    doc_vectors_count[:, members[0]] = (
                        np.sum(doc_vectors_count[:, members[1:]], axis=1)
                    )
                    tokens_to_delete += members[1:]
            tokens_to_keep = list(
                set(np.arange(doc_vectors_count.shape[1]))
                - set(tokens_to_delete)
            )
            doc_vectors_count = doc_vectors_count[:, tokens_to_keep]
            idx_2_token = [token
                           for idx, token in enumerate(idx_2_token)
                           if idx in tokens_to_keep]
    else:
        count_vectorizer = CountVectorizer(
            ngram_range=(1, 1), min_df=2, max_df=1.0, stop_words='english'
        )
        doc_vectors_count = count_vectorizer.fit_transform(docs)
        idx_2_token = count_vectorizer.get_feature_names()

    if tfidf:
        tfidf_transformer = TfidfTransformer(norm='l2', use_idf=True)
        doc_vectors_tfidf = tfidf_transformer.fit_transform(doc_vectors_count)
        return doc_vectors_tfidf, idx_2_token
    return doc_vectors_count, idx_2_token


def dense_vectorize(docs):
    """Given some docs, transforms them into glove vector form via spacy."""
    vectors = []
    for doc in docs:
        consider = doc.split(' ')[:configs.MAX_WORDS_TO_CONSIDER]
        if consider:
            consider = ' '.join(consider)
            vector = NLP(consider, disable=['ner', 'parser', 'tagger']).vector
        else:
            vector = np.zeros(300)
        vectors.append(vector)
    vectors = np.array(vectors)
    return vectors


def vocab_to_pos(vocab):
    """Determine POS for each token in a vocab (a list of strings)."""
    n = len(vocab)
    batch_size = 10000
    pos = []

    # use whitespace tokenizer
    old_tokenizer = NLP.tokenizer
    NLP.tokenizer = WhitespaceTokenizer(NLP.vocab)

    for start_idx in np.arange(0, n, batch_size):
        end_idx = min(start_idx + batch_size, n)
        vocab_str = ' '.join(vocab[start_idx:end_idx])
        pos += [spacy_token.pos_ for spacy_token in NLP(vocab_str)]

    # remove whitespace tokenizer
    NLP.tokenizer = old_tokenizer

    return pos
