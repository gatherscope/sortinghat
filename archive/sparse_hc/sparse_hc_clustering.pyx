cimport cython
from cython.operator cimport postincrement as postinc, dereference as deref
from libc.stdio cimport *
from libc.stdlib cimport malloc, free
from libcpp.algorithm cimport sort
from libcpp.pair cimport pair
from libcpp.set cimport set
from libcpp.unordered_map cimport unordered_map
from libcpp.unordered_set cimport unordered_set
from libcpp.vector cimport vector
cimport numpy as np

from hashlib import sha1
import numpy as np
import os
import shutil


# ~~~~~ DATA STRUCTURES ~~~~~
cdef extern from *:
    """
    struct dist_tuple
    {
        double dist;
        size_t i;
        size_t j;

        bool operator==(const struct dist_tuple& other) const
        {
            if((dist == other.dist)
                && (i == other.i)
                && (j == other.j))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        bool operator<(const struct dist_tuple& other) const
        {
            if((dist < other.dist)
                || ((dist == other.dist) && (i < other.i))
                || ((dist == other.dist) && (i == other.i) && (j < other.j)))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    };
    """
    struct dist_tuple:
        double dist
        size_t i, j


cdef dist_tuple MIN_DIST_TUPLE
MIN_DIST_TUPLE.dist = -float('inf')
MIN_DIST_TUPLE.i = 0
MIN_DIST_TUPLE.j = 0


cdef dist_tuple MAX_DIST_TUPLE
MAX_DIST_TUPLE.dist = float('inf')
MAX_DIST_TUPLE.i = 0
MAX_DIST_TUPLE.j = 0


cdef struct candidate_connection:
    size_t i, j, n_dists, complete_dists
    bint is_complete
    double sum_dists, lb_avg_dist
# ~~~~~ END DATA STRUCTURES ~~~~~


#~~~~~ UTILS ~~~~~
cdef size_t cpf(size_t x, size_t y):
    # cantor pairing function
    if x < y:
      return ((x + y) * (x + y + 1)) // 2 + y
    else:
      return ((y + x) * (y + x + 1)) // 2 + x
#~~~~~ END UTILS ~~~~~


cdef class SparseHC(object):
    # high-level variables
    cdef public double[:, :] obs_matrix
    cdef public size_t total_obs, dim
    cdef public str storage_dir

    # clustering variables
    cdef vector[FILE *] partial_sorted_obs_dists_streams
    cdef vector[dist_tuple] mergesort_dist_tuples
    cdef double max_obs_dist
    cdef vector[size_t] obs_current_clusters, magnitudes
    cdef public vector[pair[Py_ssize_t, Py_ssize_t]] children
    cdef unordered_map[size_t, vector[size_t]] descendent_obs
    cdef size_t next_cluster_id
    cdef unordered_map[size_t, unordered_set[size_t]] candidate_pairings
    cdef unordered_map[size_t, candidate_connection] candidate_connections
    cdef set[dist_tuple] complete_candidates
    cdef double min_incomplete_dist


    def __cinit__(self, double[:, :] obs_matrix):
        self.obs_matrix = obs_matrix
        self.total_obs = len(obs_matrix)
        self.dim = len(obs_matrix[0])


    @cython.cdivision(True)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def fit(self, size_t final_clusters=1, double psods_return_max_mb=500.0, double start_max_cq_size_mb=5.0):
        # clear structures
        self.partial_sorted_obs_dists_streams.clear()
        self.mergesort_dist_tuples.clear()
        self.obs_current_clusters.clear()
        self.magnitudes.clear()
        self.children.clear()
        self.descendent_obs.clear()
        self.candidate_pairings.clear()
        self.candidate_connections.clear()
        self.complete_candidates.clear()

        # initialize observation distances data with which to cluster observations
        self.storage_dir = self.write_partial_sorted_obs_dists(
            self.obs_matrix,
            return_max_mb=psods_return_max_mb
        )
        self._initialize_mergesort()


        # initialize clustering structures
        cdef size_t total_clusters = self.total_obs*2-final_clusters
        cdef size_t i
        for i from 0 <= i < self.total_obs:
            self.obs_current_clusters.push_back(i)
            self.descendent_obs[i] = [i]
        for i from 0 <= i < total_clusters:
            if i < self.total_obs:
                self.magnitudes.push_back(1)
            else:
                self.magnitudes.push_back(0)
            self.children.push_back((-1, -1))

        # define variables used below
        cdef size_t obs_pairs_popped, start_obs_pairs_popped, obs_0, obs_1
        cdef double max_cq_size_mb, obs_pair_dist, updated_lb_avg_dist
        cdef dist_tuple obs_dist_tuple

        # build clusters
        max_cq_size_mb = start_max_cq_size_mb
        self.next_cluster_id = self.total_obs * 1
        obs_pairs_popped = 0
        self.max_obs_dist = -float('inf')
        self.min_incomplete_dist = float('inf')
        while self.next_cluster_id < total_clusters:
            start_obs_pairs_popped = obs_pairs_popped * 1
            while ((self.mergesort_dist_tuples.size() > 0)
                and ((sizeof(set[dist_tuple]) + self.complete_candidates.size() * sizeof(dist_tuple)) / 1e6 < max_cq_size_mb)):
                obs_dist_tuple = self._mergesort_pop_next_dt()
                obs_pair_dist = obs_dist_tuple.dist
                cluster_0 = self.obs_current_clusters.at(obs_dist_tuple.i)
                cluster_1 = self.obs_current_clusters.at(obs_dist_tuple.j)
                self.max_obs_dist = max(self.max_obs_dist, obs_pair_dist)
                self._update_candidate_connections(cluster_0, cluster_1, obs_pair_dist, 1)
                obs_pairs_popped += 1
            if obs_pairs_popped == start_obs_pairs_popped: # if cq size limit too small, increase it
                max_cq_size_mb *= 1.5
            else:
                if self.min_incomplete_dist != float('inf'): # only calc min_incomplete_dist if there are incompletes
                    self._update_min_incomplete_dist(True)
                while ((self.complete_candidates.size() > 0)
                      and (deref(self.complete_candidates.begin()).dist <= self.min_incomplete_dist)):
                    self._merge_clusters(deref(self.complete_candidates.begin()))
                    self.complete_candidates.erase(self.complete_candidates.begin())
                    self.next_cluster_id += 1

        shutil.rmtree(self.storage_dir)


    @cython.cdivision(True)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def fit_no_icq(self, size_t final_clusters=1, double psods_return_max_mb=500.0):
        # clear structures
        self.partial_sorted_obs_dists_streams.clear()
        self.mergesort_dist_tuples.clear()
        self.obs_current_clusters.clear()
        self.magnitudes.clear()
        self.children.clear()
        self.descendent_obs.clear()
        self.candidate_pairings.clear()
        self.candidate_connections.clear()
        self.complete_candidates.clear()

        # initialize observation distances data with which to cluster observations
        self.storage_dir = self.write_partial_sorted_obs_dists(
            self.obs_matrix,
            return_max_mb=psods_return_max_mb
        )
        self._initialize_mergesort()

        # initialize clustering structures
        cdef size_t total_clusters = self.total_obs*2-final_clusters
        cdef size_t i
        for i from 0 <= i < self.total_obs:
            self.obs_current_clusters.push_back(i)
            self.descendent_obs[i] = [i]
        for i from 0 <= i < total_clusters:
            if i < self.total_obs:
                self.magnitudes.push_back(1)
            else:
                self.magnitudes.push_back(0)
            self.children.push_back((-1, -1))

        # define variables used below
        cdef size_t obs_0, obs_1
        cdef double max_cq_size_mb, obs_pair_dist
        cdef dist_tuple obs_dist_tuple

        # build clusters
        self.next_cluster_id = self.total_obs * 1
        self.max_obs_dist = -float('inf')
        while self.next_cluster_id < total_clusters:
            obs_dist_tuple = self._mergesort_pop_next_dt()
            obs_pair_dist = obs_dist_tuple.dist
            cluster_0 = self.obs_current_clusters.at(obs_dist_tuple.i)
            cluster_1 = self.obs_current_clusters.at(obs_dist_tuple.j)
            self.max_obs_dist = max(self.max_obs_dist, obs_pair_dist)
            self._update_candidate_connections(cluster_0, cluster_1, obs_pair_dist, 1)
            while self.complete_candidates.size() > 0:
                self._merge_clusters(deref(self.complete_candidates.begin()))
                self.complete_candidates.erase(self.complete_candidates.begin())
                self.next_cluster_id += 1
        shutil.rmtree(self.storage_dir)


    # TODO: move to utils
    @cython.cdivision(True)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def write_partial_sorted_obs_dists(self, double[:, :] obs_matrix, double return_max_mb=500.0):
        cdef size_t total_obs = len(obs_matrix)
        cdef size_t dim = len(obs_matrix[0])
        cdef vector[dist_tuple] obs_dists
        cdef size_t n_to_return = int(
            (return_max_mb - sizeof(vector[dist_tuple])*1e-6)
                // (sizeof(dist_tuple)*1e-6)
        )
        if not os.path.exists('dist_tuples_mergesorting'):
            os.mkdir('dist_tuples_mergesorting')
        cdef str storage_dir = 'dist_tuples_mergesorting/' + sha1(np.asarray(obs_matrix)).hexdigest()
        if not os.path.exists(storage_dir):
            os.mkdir(storage_dir)

        cdef size_t i, j, k
        cdef size_t output_ct = 0
        cdef double dist
        cdef dist_tuple obs_dist_tuple

        # For each observation in obs_matrix, calculate euclidean distance
        # w/r/t all other observations (except those already calculated).
        # If appropriate, add pair and distance to partial_sorted_dist_heap.
        for i from 0 <= i < total_obs:
            for j from i+1 <= j < total_obs:
                dist = 0
                for k from 0 <= k < dim:
                    dist += (obs_matrix[i, k] - obs_matrix[j, k])**2
                dist = dist ** 0.5
                obs_dist_tuple.dist = dist
                obs_dist_tuple.i = i
                obs_dist_tuple.j = j
                obs_dists.push_back(obs_dist_tuple)
                if ((obs_dists.size() == n_to_return)
                    or ((i == (total_obs - 2)) and (j == (total_obs - 1)))):
                    sort(obs_dists.begin(), obs_dists.end())
                    filename = storage_dir + '/dist_tuples_'+str(output_ct)+'.bin'
                    self._write_dist_tuples(obs_dists, filename)
                    obs_dists.clear()
                    output_ct += 1

        return storage_dir


    # TODO: move to utils
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void _write_dist_tuples(self, vector[dist_tuple] dist_tuples, str filename):
        cdef bytes filename_byte_string = filename.encode('UTF-8')
        cdef char* fname = filename_byte_string
        cdef size_t N, i
        N = dist_tuples.size()
        cdef dist_tuple *ptr_d = <dist_tuple *>malloc(N * sizeof(dist_tuple))
        for i from 0 <= i < N:
            ptr_d[i] = dist_tuples[i]
        cdef FILE *ptr_fw
        ptr_fw = fopen(fname, "wb")
        fwrite(ptr_d, sizeof(dist_tuple), N, ptr_fw)
        fclose(ptr_fw)
        free(ptr_d)


    # TODO: move to utils
    @cython.cdivision(True)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef vector[dist_tuple] _read_dist_tuples(self, str filename):
        cdef bytes filename_byte_string = filename.encode('UTF-8')
        cdef char* fname = filename_byte_string
        cdef FILE *ptr_fr
        cdef size_t fsize, N, i
        ptr_fr = fopen(fname, "rb")
        fseek(ptr_fr, 0, SEEK_END)
        fsize = ftell(ptr_fr)
        N = fsize // sizeof(dist_tuple)
        rewind(ptr_fr)
        cdef Py_ssize_t result
        cdef dist_tuple *ptr_d = <dist_tuple *>malloc(N * sizeof(dist_tuple))
        result = fread(ptr_d, sizeof(dist_tuple), N, ptr_fr)
        fclose(ptr_fr)
        cdef vector[dist_tuple] dist_tuples
        for i in range(N):
            dist_tuples.push_back(ptr_d[i])
        free(ptr_d)
        return dist_tuples


    # TODO: move to utils
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void _initialize_mergesort(self):
        cdef size_t i
        cdef size_t n_files = len([f for f in os.listdir(self.storage_dir) if '.bin' in f])
        cdef str filename
        cdef bytes filename_byte_string
        cdef char* fname
        cdef Py_ssize_t result
        cdef dist_tuple *ptr_d = <dist_tuple *>malloc(sizeof(dist_tuple))
        for i in range(n_files):
            filename = (self.storage_dir+'/dist_tuples_'+str(i)+'.bin')
            filename_byte_string = filename.encode('UTF-8')
            fname = filename_byte_string
            self.partial_sorted_obs_dists_streams.push_back(fopen(fname, "rb"))
            result = fread(ptr_d, sizeof(dist_tuple), 1, self.partial_sorted_obs_dists_streams[i])
            self.mergesort_dist_tuples.push_back(ptr_d[0])
        free(ptr_d)


    # TODO: move to utils
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef dist_tuple _mergesort_pop_next_dt(self):
        cdef dist_tuple min_dt, comp_dt
        cdef size_t min_dt_pos, i
        cdef Py_ssize_t result
        cdef dist_tuple *ptr_d = <dist_tuple *>malloc(sizeof(dist_tuple))
        min_dt = MAX_DIST_TUPLE
        for i in range(self.mergesort_dist_tuples.size()):
            comp_dt = self.mergesort_dist_tuples[i]
            if comp_dt < min_dt:
                min_dt = comp_dt
                min_dt_pos = i
        result = fread(ptr_d, sizeof(dist_tuple), 1, self.partial_sorted_obs_dists_streams[min_dt_pos])
        if result == 1:
            self.mergesort_dist_tuples[min_dt_pos] = ptr_d[0]
        else:
            fclose(self.partial_sorted_obs_dists_streams[min_dt_pos])
            self.partial_sorted_obs_dists_streams.erase(self.partial_sorted_obs_dists_streams.begin()+min_dt_pos)
            self.mergesort_dist_tuples.erase(self.mergesort_dist_tuples.begin()+min_dt_pos)
        free(ptr_d)
        return min_dt


    @cython.cdivision(True)
    cdef void _update_candidate_connections(self, size_t cluster_0, size_t cluster_1,
                                            double sum_dists_update, size_t n_dists_update):
        cdef size_t cc_id = cpf(cluster_0, cluster_1)
        cdef candidate_connection connection
        cdef dist_tuple cluster_dist_tuple
        cdef unordered_set[size_t] candidate_pairings_init
        cluster_dist_tuple.i = min(cluster_0, cluster_1)
        cluster_dist_tuple.j = max(cluster_0, cluster_1)

        # if candidate connection already exists between clusters, update it
        if self.candidate_connections.count(cc_id) > 0:
            connection = self.candidate_connections[cc_id]
            connection.sum_dists += sum_dists_update
            connection.n_dists += n_dists_update
            connection.is_complete = (
                connection.n_dists == connection.complete_dists
            )
            connection.lb_avg_dist = (
                (connection.sum_dists
                 + self.max_obs_dist
                 * (connection.complete_dists - connection.n_dists))
                / connection.complete_dists
            )
        # otherwise create a new connection
        else:
            connection.i = cluster_dist_tuple.i
            connection.j = cluster_dist_tuple.j
            connection.sum_dists = sum_dists_update
            connection.n_dists = n_dists_update
            connection.complete_dists = self.magnitudes.at(cluster_0) * self.magnitudes.at(cluster_1)
            connection.is_complete = (
                connection.n_dists == connection.complete_dists
            )
            connection.lb_avg_dist = (
                (connection.sum_dists
                 + self.max_obs_dist
                 * (connection.complete_dists - connection.n_dists))
                / connection.complete_dists
            )
            if self.candidate_pairings.count(cluster_0) > 0:
                self.candidate_pairings[cluster_0].insert(cluster_1)
            else:
                candidate_pairings_init.clear()
                candidate_pairings_init.insert(cluster_1)
                self.candidate_pairings[cluster_0] = candidate_pairings_init

            if self.candidate_pairings.count(cluster_1) > 0:
                self.candidate_pairings[cluster_1].insert(cluster_0)
            else:
                candidate_pairings_init.clear()
                candidate_pairings_init.insert(cluster_0)
                self.candidate_pairings[cluster_1] = candidate_pairings_init
        # insert connection
        self.candidate_connections[cc_id] = connection
        # update queues
        cluster_dist_tuple.dist = connection.lb_avg_dist
        if connection.is_complete:
            self.complete_candidates.insert(cluster_dist_tuple)
        else:
            self.min_incomplete_dist = min(self.min_incomplete_dist, cluster_dist_tuple.dist)


    @cython.cdivision(True)
    cdef void _update_min_incomplete_dist(self, bint write):
      cdef unordered_map[size_t, candidate_connection].iterator it = self.candidate_connections.begin()
      cdef size_t cc_id
      cdef candidate_connection connection
      self.min_incomplete_dist = float('inf')
      while it != self.candidate_connections.end():
          connection = deref(it).second
          if not connection.is_complete:
              if write:
                  cc_id = deref(it).first
                  connection.lb_avg_dist = (
                      (connection.sum_dists
                       + self.max_obs_dist
                       * (connection.complete_dists - connection.n_dists))
                      / connection.complete_dists
                  )
                  self.candidate_connections[cc_id] = connection
              self.min_incomplete_dist = min(self.min_incomplete_dist, connection.lb_avg_dist)
          postinc(it)


    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void _merge_clusters(self, dist_tuple cluster_dist_tuple):
        cdef size_t cluster_0 = cluster_dist_tuple.i
        cdef size_t cluster_1 = cluster_dist_tuple.j

        # merge magnitudes
        cdef size_t next_cluster_magnitude = self.magnitudes.at(cluster_0) + self.magnitudes.at(cluster_1)
        self.magnitudes[self.next_cluster_id] = next_cluster_magnitude

        # merge descendent observations
        self.descendent_obs[cluster_0].insert(
            self.descendent_obs[cluster_0].end(),
            self.descendent_obs[cluster_1].begin(),
            self.descendent_obs[cluster_1].end(),
        )
        self.descendent_obs[self.next_cluster_id] = self.descendent_obs[cluster_0]

        # redirect descendent observation clusters
        cdef size_t obs
        for obs in self.descendent_obs[self.next_cluster_id]:
            self.obs_current_clusters[obs] = self.next_cluster_id

        # set children
        cdef pair[Py_ssize_t, Py_ssize_t] children_pair
        children_pair.first = cluster_0
        children_pair.second = cluster_1
        self.children[self.next_cluster_id] = children_pair

        # rewire merged-clusters candidate connections toward new cluster
        cdef unordered_set[size_t] cp_0 = self.candidate_pairings[cluster_0]
        self.candidate_pairings.erase(cluster_0)
        cdef unordered_set[size_t] cp_1 = self.candidate_pairings[cluster_1]
        self.candidate_pairings.erase(cluster_1)
        cdef unordered_set[size_t] candidate_clusters
        cdef size_t cluster, n_dists, cc_id
        cdef double sum_dists
        cdef dist_tuple old_cluster_dist_tuple
        cdef candidate_connection old_connection, new_connection
        for cluster in cp_0:
            candidate_clusters.insert(cluster)
        for cluster in cp_1:
            candidate_clusters.insert(cluster)
        candidate_clusters.erase(cluster_0)
        candidate_clusters.erase(cluster_1)
        for cluster in candidate_clusters:
            sum_dists = 0
            n_dists = 0
            if cp_0.count(cluster) > 0:
                # TODO: separate this into method
                cc_id = cpf(cluster, cluster_0)
                old_connection = self.candidate_connections[cc_id]
                sum_dists += old_connection.sum_dists
                n_dists += old_connection.n_dists
                old_cluster_dist_tuple.i = old_connection.i
                old_cluster_dist_tuple.j = old_connection.j
                old_cluster_dist_tuple.dist = old_connection.lb_avg_dist
                if old_connection.is_complete:
                    self.complete_candidates.erase(old_cluster_dist_tuple)
                elif old_connection.lb_avg_dist == self.min_incomplete_dist:
                    self._update_min_incomplete_dist(False)
                self.candidate_connections.erase(cc_id)
                self.candidate_pairings[cluster].erase(cluster_0)
            if cp_1.count(cluster) > 0:
                cc_id = cpf(cluster, cluster_1)
                old_connection = self.candidate_connections[cc_id]
                sum_dists += old_connection.sum_dists
                n_dists += old_connection.n_dists
                old_cluster_dist_tuple.i = old_connection.i
                old_cluster_dist_tuple.j = old_connection.j
                old_cluster_dist_tuple.dist = old_connection.lb_avg_dist
                if old_connection.is_complete:
                    self.complete_candidates.erase(old_cluster_dist_tuple)
                elif old_connection.lb_avg_dist == self.min_incomplete_dist:
                    self._update_min_incomplete_dist(False)
                self.candidate_connections.erase(cc_id)
                self.candidate_pairings[cluster].erase(cluster_1)
            self._update_candidate_connections(cluster, self.next_cluster_id,
                                               sum_dists, n_dists)
        cc_id = cpf(cluster_0, cluster_1)
        self.candidate_connections.erase(cc_id)
