from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

extensions = [
    Extension(
        "sparse_hc_clustering",
        ["sparse_hc_clustering.pyx"],
        language="c++",
        extra_compile_args=["-std=c++14", "-stdlib=libc++"],
        extra_link_args=["-std=c++14"],
        include_dirs=[np.get_include()],
        annotate=True
    )
]

setup(
    ext_modules=cythonize(extensions)
)
