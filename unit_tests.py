from datetime import datetime, timezone
import time
import unittest

from dateutil.parser import parse as timeparse
import numpy as np
from numpy import ndarray
import pandas as pd
from scipy.sparse.csr import csr_matrix

from categorization import categorize_by_keywords,\
                           svm_extrapolate_categorization
from clustering import form_topic_clusters, get_same_cluster_probs,\
                       agglomeratively_cluster_dist_matrix, gen_clusters,\
                       form_subtopic_clusters
from collection import get_publisher_feeds, get_recently_crawled_content,\
                       include_title, clean
import configs
from content_corpus import ContentCorpus
from vectorization import lemmatize, simple_split, vectorize, dense_vectorize,\
                          vocab_to_pos


class LoadingMethods(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_include_title(self):
        title = 'Honda to shutter UK factory amid electric cars push'
        self.assertTrue(include_title(title))

        title = 'Google Earnings - Call Transcript'
        self.assertFalse(include_title(title))

    def test_clean(self):
        text = 'This is a test sentence.'
        actual_cleaned = clean(text)
        intended_cleaned = 'This is a test sentence'
        self.assertEqual(actual_cleaned, intended_cleaned)

    def test_get_publisher_feeds(self):
        feeds = get_publisher_feeds()
        cols = feeds.columns
        n = len(feeds)
        self.assertTrue('publisher' in cols)
        self.assertTrue('feed_url' in cols)
        self.assertTrue('local_categorization' in cols)
        self.assertTrue('region_categorization' in cols)
        self.assertTrue('topic_categorization' in cols)
        self.assertTrue('quality_score' in cols)
        self.assertGreater(n, 10)

    def test_get_recently_crawled_content(self):
        feeds = get_publisher_feeds()
        rc, rcc, ir = get_recently_crawled_content(list(feeds['feed_url']),
                                                   0.042)  # last hour
        cols = rc.columns
        n = len(rc)
        self.assertTrue('_id' in cols)
        self.assertTrue('crawled_timestamp' in cols)
        self.assertTrue('feed_url' in cols)
        self.assertGreater(n, 10)

    def test_create_class(self):
        cc = ContentCorpus()
        self.assertIsNone(cc.city_id)
        cc = ContentCorpus(city_id=1)
        self.assertEqual(cc.city_id, 1)

    def test_load_recent_content_from_file(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        df = df.sample(frac=0.25).reset_index()  # speed
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cols = cc.df.columns
        n = len(cc.df)
        self.assertTrue('_id' in cols)
        self.assertTrue('crawled_timestamp' in cols)
        self.assertGreater(n, 10)

    def test_load_recent_content_from_db(self):
        cc = ContentCorpus()
        cc.load_recent_content(days_of_recent_content=0.042)  # last hour
        cols = cc.df.columns
        n = len(cc.df)
        self.assertTrue('_id' in cols)
        self.assertTrue('crawled_timestamp' in cols)
        self.assertGreater(n, 10)


class VectorizationMethods(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_lemmatize(self):
        text = 'The quick brown fox jumps over the lazy dog.'
        actual_lemmatized = lemmatize(text)
        intended_lemmatized = 'quick brown fox jump lazy dog'
        self.assertEqual(actual_lemmatized, intended_lemmatized)

        actual_lemmatized = lemmatize(text, pos_to_keep=['ADJ'])
        intended_lemmatized = 'quick brown lazy'
        self.assertEqual(actual_lemmatized, intended_lemmatized)

        actual_lemmatized = lemmatize(text, single_str_output=False)
        intended_lemmatized = ['quick', 'brown', 'fox', 'jump', 'lazy', 'dog']
        self.assertEqual(actual_lemmatized, intended_lemmatized)

        actual_lemmatized = lemmatize(text, incl_pos=True)
        intended_lemmatized = (
            'quick_ADJ brown_ADJ fox_NOUN jump_VERB lazy_ADJ dog_NOUN'
        )
        self.assertEqual(actual_lemmatized, intended_lemmatized)

        actual_lemmatized, vectors = lemmatize(text, return_vectors=True)
        intended_lemmatized = 'quick brown fox jump lazy dog'
        self.assertEqual(actual_lemmatized, intended_lemmatized)
        self.assertEqual(vectors[0].shape, (300,))

    def test_simple_split(self):
        text = 'This is true.'
        actual_split = simple_split(text)
        intended_split = ['This', 'is', 'true.']
        self.assertEqual(actual_split, intended_split)

    def test_vectorize(self):
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        docs = df['title'][:100]
        vectors, idx_2_token = vectorize(docs)
        self.assertEqual(vectors.shape[0], 100)
        self.assertGreater(vectors.shape[1], 10)
        self.assertGreater(len(idx_2_token), 10)

    def test_dense_vectorize(self):
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        docs = df['title'][:100]
        vectors = dense_vectorize(docs)
        self.assertEqual(vectors.shape[0], 100)
        self.assertEqual(vectors.shape[1], 300)

    def test_vocab_to_pos(self):
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        docs = df['title'][:100]
        vectors, idx_2_token = vectorize(docs)
        idx_2_pos = vocab_to_pos(idx_2_token)
        self.assertTrue('NOUN' in idx_2_pos)

    def test_vectorize_content(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        self.assertIsInstance(cc.titles_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.titles_tfidf_vocab, list)
        self.assertIsInstance(cc.titles_dense_matrix, ndarray)
        self.assertIsInstance(cc.texts_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.texts_tfidf_vocab, list)
        self.assertIsInstance(cc.texts_tfidf_pos, ndarray)
        self.assertIsInstance(cc.texts_dense_matrix, ndarray)
        self.assertIsInstance(cc.texts_nouns_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.texts_adjs_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.meta_keywords_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.meta_keywords_tfidf_vocab, list)
        self.assertIsInstance(cc.master_tfidf_matrix, csr_matrix)
        self.assertIsInstance(cc.master_tfidf_vocab, list)


class CategorizationMethods(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_categorize_by_keywords(self):
        text = 'science & technology'
        actual_categorization = categorize_by_keywords(
            text,
            configs.TOPIC_CATEGORIZATIONS_AND_KEYWORDS
        )
        intended_categorization = 'Technology, Science & Medicine'
        self.assertEqual(actual_categorization, intended_categorization)

        text = 'austria'
        actual_categorization = categorize_by_keywords(
            text,
            configs.REGION_CATEGORIZATIONS_AND_KEYWORDS
        )
        intended_categorization = 'Europe'
        self.assertEqual(actual_categorization, intended_categorization)

        text = 'ytrewq'
        actual_categorization = categorize_by_keywords(
            text,
            configs.TOPIC_CATEGORIZATIONS_AND_KEYWORDS
        )
        self.assertIsNone(actual_categorization)

    def test_svm_extrapolate_categorization(self):
        X = np.random.rand(1000)
        y = np.array([1 if x < 0.25 else 0 for x in X])
        df = pd.DataFrame({
            'y': y,
            'X': X
        })
        df = svm_extrapolate_categorization(df, X.reshape(-1, 1), 'y')
        recall = (
            np.sum(df['y_predicted'] & df['y'])
            / np.sum(df['y'])
        )
        self.assertGreater(recall, 0.8)
        precision = (
            np.sum(df['y_predicted'] & df['y'])
            / np.sum(df['y_predicted'])
        )
        self.assertGreater(precision, 0.8)

    def test_categorize_content(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        cc.categorize_content()
        recall = (
            np.sum(
                (cc.df['topic_categorization_predicted']
                 == 'Politics, Policy & Economics')
                &
                (cc.df['topic_categorization']
                 == 'Politics, Policy & Economics')
            )
            / np.sum(
                (cc.df['topic_categorization']
                 == 'Politics, Policy & Economics')
            )
        )
        self.assertGreater(recall, 0.8)
        precision = (
            np.sum(
                (cc.df['topic_categorization_predicted']
                 == 'Politics, Policy & Economics')
                &
                (cc.df['topic_categorization']
                 == 'Politics, Policy & Economics')
            )
            / np.sum(
                (cc.df['topic_categorization_predicted']
                 == 'Politics, Policy & Economics')
                & cc.df['topic_categorization'].apply(
                    lambda x: isinstance(x, str)
                )
            )
        )
        self.assertGreater(precision, 0.8)


class ClusteringMethods(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_get_same_cluster_probs(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        cc.categorize_content()

        topic = 'Politics, Policy & Economics'
        region = 'U.S. & Canada'
        filt = (
            (cc.df['topic_categorization_predicted']
             == topic)
            & (cc.df['topic_categorization_probability']
               >= configs.MIN_TOPIC_PROB)
            & (cc.df['region_categorization_predicted']
               == region)
            & (cc.df['region_categorization_probability']
               >= configs.MIN_REGION_PROB)
        )
        df_subset = cc.df[filt]
        indices = df_subset.index.values
        df_subset = df_subset.reset_index()
        n_articles = len(df_subset)

        same_cluster_probs = get_same_cluster_probs(
            'topic',  df_subset,
            cc.titles_tfidf_matrix[indices], cc.texts_tfidf_matrix[indices],
            cc.texts_nouns_tfidf_matrix[indices],
            cc.texts_adjs_tfidf_matrix[indices],
            cc.titles_dense_matrix[indices], cc.texts_dense_matrix[indices]
        )

        self.assertEqual(same_cluster_probs.shape, (n_articles, n_articles))

    def test_agglomeratively_cluster_dist_matrix(self):
        X = np.random.rand(100, 100)
        C = csr_matrix(np.ones((100, 100), dtype='bool'))

        ac_result = agglomeratively_cluster_dist_matrix(X, C)

        self.assertEqual(ac_result.shape, (99, 3))

    def test_gen_clusters(self):
        X = np.random.rand(100, 100)
        C = csr_matrix(np.ones((100, 100), dtype='bool'))

        ac_result = agglomeratively_cluster_dist_matrix(X, C)
        clusters = gen_clusters(ac_result, 5)
        n_unique = len(np.unique(clusters))

        self.assertEqual(n_unique, 5)

    def test_form_topic_clusters(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        cc.categorize_content()

        topic = 'Politics, Policy & Economics'
        region = 'U.S. & Canada'
        filt = (
            (cc.df['topic_categorization_predicted']
             == topic)
            & (cc.df['topic_categorization_probability']
               >= configs.MIN_TOPIC_PROB)
            & (cc.df['region_categorization_predicted']
               == region)
            & (cc.df['region_categorization_probability']
               >= configs.MIN_REGION_PROB)
        )
        df_subset = cc.df[filt]
        indices = df_subset.index.values
        df_subset = df_subset.reset_index()

        matrices = {
            'titles_tfidf_matrix':
                cc.titles_tfidf_matrix[indices],
            'texts_tfidf_matrix':
                cc.texts_tfidf_matrix[indices],
            'texts_nouns_tfidf_matrix':
                cc.texts_nouns_tfidf_matrix[indices],
            'texts_adjs_tfidf_matrix':
                cc.texts_adjs_tfidf_matrix[indices],
            'titles_dense_matrix':
                cc.titles_dense_matrix[indices],
            'texts_dense_matrix':
                cc.texts_dense_matrix[indices]
        }
        batch_timestamp = (
            datetime.utcnow().replace(tzinfo=timezone.utc)
        )
        batch_type = (
            'recent %sd' % str(cc.days_of_recent_content)
        )
        df_subset = form_topic_clusters(
            df_subset, matrices, batch_timestamp, batch_type,
            return_df=True, call_form_subtopic_clusters=False,
            verbose=False
        )
        cols = df_subset.columns
        self.assertTrue('topic_cluster_id' in cols)

    def test_form_subtopic_clusters(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        cc.categorize_content()

        topic = 'Politics, Policy & Economics'
        region = 'U.S. & Canada'
        filt = (
            (cc.df['topic_categorization_predicted']
             == topic)
            & (cc.df['topic_categorization_probability']
               >= configs.MIN_TOPIC_PROB)
            & (cc.df['region_categorization_predicted']
               == region)
            & (cc.df['region_categorization_probability']
               >= configs.MIN_REGION_PROB)
            & (cc.df['title'].apply(lambda x: 'treaty' in x.lower()))
        )
        df_subset = cc.df[filt]
        indices = df_subset.index.values
        df_subset = df_subset.reset_index()

        matrices = {
            'titles_tfidf_matrix':
                cc.titles_tfidf_matrix[indices],
            'texts_tfidf_matrix':
                cc.texts_tfidf_matrix[indices],
            'texts_nouns_tfidf_matrix':
                cc.texts_nouns_tfidf_matrix[indices],
            'texts_adjs_tfidf_matrix':
                cc.texts_adjs_tfidf_matrix[indices],
            'titles_dense_matrix':
                cc.titles_dense_matrix[indices],
            'texts_dense_matrix':
                cc.texts_dense_matrix[indices]
        }
        batch_timestamp = (
            datetime.utcnow().replace(tzinfo=timezone.utc)
        )
        batch_type = (
            'recent %sd' % str(cc.days_of_recent_content)
        )
        df_subset = form_subtopic_clusters(
            df_subset, matrices, batch_timestamp, batch_type,
            return_df=True, write_to_mdb=False, verbose=False
        )
        cols = df_subset.columns
        self.assertTrue('subtopic_cluster_id' in cols)

    def test_cluster_content(self):
        cc = ContentCorpus()
        df = pd.read_csv('data/recent_content_corpus.csv', index_col=0)
        df = df.sample(frac=0.25).reset_index()  # speed
        df['crawled_timestamp'] = df['crawled_timestamp'].apply(timeparse)
        cc.load_recent_content(df=df, days_of_recent_content=2)
        cc.vectorize_content()
        cc.categorize_content()
        cc.cluster_content(enqueue=False)


if __name__ == '__main__':
    unittest.main()
