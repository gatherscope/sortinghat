from datetime import datetime
from hashlib import sha1
from heapq import heapify, heappop, heappush
import logging
import os
import pickle

import numpy as np
import pandas as pd
from pymongo import MongoClient
from scipy.sparse import csr_matrix, lil_matrix
from sklearn.cluster import _hierarchical
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.utils.fast_dict import IntFloatDict

import configs

# logging config
LOGS_PATH = configs.LOGS_PATH
if not os.path.exists(LOGS_PATH):
    os.mkdir(LOGS_PATH)
logging.basicConfig(
    filename=LOGS_PATH+'/performance.log',
    format='%(asctime)s %(message)s',
    level=logging.DEBUG
)

# MongoDB Config
MDB_CLIENT = MongoClient(configs.MDB_HOST)
SORTINGHAT_MDB = MDB_CLIENT.sortinghat_mdb
CONTENT_CLUSTERS = SORTINGHAT_MDB.content_clusters


def form_topic_clusters(df, matrices, batch_timestamp, batch_type,
                        return_df=False, call_form_subtopic_clusters=True,
                        verbose=False):
    """Clusters articles at the topic level, then proceeds to subtopic clustering

    Given a df of articles and associated matrices, forms a graph representing
        the probability that each article is in each other article's topic
        cluster which is agglomeratively clustered to form likely topic
        clusters.

    Keyword arguments:
    df -- dataset of articles to be clustered (pandas DataFrame)
    matrices -- matrices corresponding to articles from content_corpus (dict of
        numpy arrays)
    batch_timestamp -- timestamp that df clustering is initiated, to be passed
        on (datetime)
    batch_type -- type of df being clustered, to be passed on (string)
    return_df -- whether to return df with cluster labels added (bool)
    form_subtopic_clusters -- whether to run same-named method for each
        resulting topic cluster

    Returns:
    pandas DataFrame (if return_df)

    """
    n_articles = len(df)

    topic_categorization = df['topic_categorization_predicted'][0]
    region_categorization = df['region_categorization_predicted'][0]

    outstr = '\n\nBeginning topic clustering...'
    outstr += ('\nBatch TS: %s' % str(batch_timestamp))
    outstr += ('\nBatch Type: %s' % batch_type)
    outstr += ('\nTopic: %s' % topic_categorization)
    outstr += ('\nRegion: %s' % region_categorization)

    X = -get_same_cluster_probs(  # negative to support agglomerative cluster
        'topic',
        df,
        matrices['titles_tfidf_matrix'],
        matrices['texts_tfidf_matrix'],
        matrices['texts_nouns_tfidf_matrix'],
        matrices['texts_adjs_tfidf_matrix'],
        matrices['titles_dense_matrix'],
        matrices['texts_dense_matrix']
    ).tocsr()
    C = csr_matrix(X < 0, dtype='bool')
    # TODO: may need p_same_cluster_threshold to vary by topic, region category
    p_same_cluster_threshold = configs.TOPIC_MIN_P_SAME_CLUSTER

    try:
        ac_result = agglomeratively_cluster_dist_matrix(
            X, C, max_merge_dist=-p_same_cluster_threshold
        )

        # if disconnected graph with early stopping, extend ac_result with
        # dummy data for gen_clusters compatibility
        to_append = np.array(
            [[1e6, 1e6, 0]
             for i in range(n_articles - 1 - ac_result.shape[0])]
        )
        if to_append.size > 0:
            ac_result = np.vstack((ac_result, to_append))
        ac_result_dec = ac_result[::-1]

        optimal_n_clusters = (np.where(
            (-ac_result_dec[:, 2]) > p_same_cluster_threshold
        )[0][0] + 1)
    except:
        ac_result = agglomeratively_cluster_dist_matrix(X, C)

        to_append = np.array(
            [[1e6, 1e6, 0]
             for i in range(n_articles - 1 - ac_result.shape[0])]
        )
        if to_append.size > 0:
            ac_result = np.vstack((ac_result, to_append))
        ac_result_dec = ac_result[::-1]

        optimal_n_clusters = n_articles // 10

    outstr += ('\nOptimal N Clusters: %s' % (optimal_n_clusters))

    df['topic_cluster_id'] = gen_clusters(ac_result, optimal_n_clusters)

    if call_form_subtopic_clusters:
        cluster_id_counts = df['topic_cluster_id'].value_counts()
        for cluster_id, n_articles in zip(cluster_id_counts.index,
                                          cluster_id_counts.values):
            if n_articles >= configs.MIN_ARTICLES_IN_TOPIC_CLUSTER:
                filt = (df['topic_cluster_id'] == cluster_id)
                df_subset = df[filt].reset_index()
                form_subtopic_clusters(df_subset, matrices, batch_timestamp,
                                       batch_type, verbose=verbose)
    if verbose:
        print(outstr)
    logging.info(outstr)

    if return_df:
        return df


def get_same_cluster_probs(level, df, titles_tfidf_matrix, texts_tfidf_matrix,
                           texts_nouns_tfidf_matrix, texts_adjs_tfidf_matrix,
                           titles_dense_matrix, texts_dense_matrix,
                           pairs_dists_max_memory_use_mb=500):
    """Given an articles df and various matrices, forms same_cluster_probs graph.

    Keyword arguments:
    level - either 'topic' or 'subtopic' (str)
    df - articles df from content_corpus (pandas DataFrame)
    titles_tfidf_matrix - from content_corpus (numpy csr matrix)
    texts_tfidf_matrix - from content_corpus (numpy csr matrix)
    texts_nouns_tfidf_matrix - from content_corpus (numpy csr matrix)
    texts_adjs_tfidf_matrix - from content_corpus (numpy csr matrix)
    titles_dense_matrix - from content_corpus (numpy array)
    texts_dense_matrix - from content_corpus (numpy array)

    Returns:
    numpy lil_matrix - where [i, j] is the probability that articles i and j
        are in the same cluster (articles pairs w/ probability > 0 included)
    """
    n_articles = len(df)
    df['const'] = np.zeros(n_articles)
    df['n'] = np.arange(n_articles)

    if level == 'topic':
        with open(configs.TOPIC_CLUSTER_CLASSIFIER_PATH, 'rb') as input:
            cluster_classifier = pickle.load(input)
        features = configs.TOPIC_CLUSTER_CLASSIFIER_FEATURES
        dep_var = 'same_topic_cluster_probability'
    else:  # level = 'subtopic'
        with open(configs.SUBTOPIC_CLUSTER_CLASSIFIER_PATH, 'rb') as input:
            cluster_classifier = pickle.load(input)
        features = configs.SUBTOPIC_CLUSTER_CLASSIFIER_FEATURES
        dep_var = 'same_subtopic_cluster_probability'

    same_cluster_probs = lil_matrix((n_articles, n_articles))
    num_dist_matrices = 6
    per_dist_matrix_max_memory = (
        pairs_dists_max_memory_use_mb / num_dist_matrices / 2
    )
    cols_batch = int(min(n_articles, per_dist_matrix_max_memory/8e-6))
    rows_batch = int(
        min(n_articles, per_dist_matrix_max_memory/(cols_batch * 8e-6))
    )

    # TODO: can parallelize looping if needed
    # slice first articles
    for start_row in np.arange(0, n_articles, rows_batch):
        end_row = min(start_row+rows_batch, n_articles)

        # get first articles' vectors
        a1_indices = df.index.values[start_row:end_row]
        a1_titles_tfidf_matrix = titles_tfidf_matrix[a1_indices]
        a1_texts_tfidf_matrix = texts_tfidf_matrix[a1_indices]
        a1_nouns_tfidf_matrix = texts_nouns_tfidf_matrix[a1_indices]
        a1_adjs_tfidf_matrix = texts_adjs_tfidf_matrix[a1_indices]
        a1_titles_dense_matrix = titles_dense_matrix[a1_indices]
        a1_texts_dense_matrix = texts_dense_matrix[a1_indices]

        # slice second articles
        for start_col in np.arange(start_row, n_articles, cols_batch):
            end_col = min(start_col+cols_batch, n_articles)

            # get second articles' vectors
            a2_indices = df.index.values[start_col:end_col]
            a2_titles_tfidf_matrix = titles_tfidf_matrix[a2_indices]
            a2_texts_tfidf_matrix = texts_tfidf_matrix[a2_indices]
            a2_nouns_tfidf_matrix = texts_nouns_tfidf_matrix[a2_indices]
            a2_adjs_tfidf_matrix = texts_adjs_tfidf_matrix[a2_indices]
            a2_titles_dense_matrix = titles_dense_matrix[a2_indices]
            a2_texts_dense_matrix = texts_dense_matrix[a2_indices]

            # create article pairs distance matrices
            titles_dists = 1-cosine_similarity(a1_titles_tfidf_matrix,
                                               a2_titles_tfidf_matrix)
            texts_dists = 1-cosine_similarity(a1_texts_tfidf_matrix,
                                              a2_texts_tfidf_matrix)
            nouns_dists = 1-cosine_similarity(a1_nouns_tfidf_matrix,
                                              a2_nouns_tfidf_matrix)
            adjs_dists = 1-cosine_similarity(a1_adjs_tfidf_matrix,
                                             a2_adjs_tfidf_matrix)
            titles_dense_dists = 1-cosine_similarity(a1_titles_dense_matrix,
                                                     a2_titles_dense_matrix)
            texts_dense_dists = 1-cosine_similarity(a1_texts_dense_matrix,
                                                    a2_texts_dense_matrix)

            # conserve memory
            del a2_indices, a2_titles_tfidf_matrix, a2_texts_tfidf_matrix,\
                a2_nouns_tfidf_matrix, a2_adjs_tfidf_matrix,\
                a2_titles_dense_matrix, a2_texts_dense_matrix

            # combine article pairs distances
            pairs_metrics = pd.merge(
                df[['const', 'n']][start_row:end_row],
                df[['const', 'n']][start_col:end_col],
                on='const'
            )
            pairs_metrics['title_dist'] = titles_dists.reshape(-1)
            pairs_metrics['text_dist'] = texts_dists.reshape(-1)
            pairs_metrics['text_nouns_dist'] = nouns_dists.reshape(-1)
            pairs_metrics['text_adjs_dist'] = adjs_dists.reshape(-1)
            pairs_metrics['title_dense_dist'] = titles_dense_dists.reshape(-1)
            pairs_metrics['text_dense_dist'] = texts_dense_dists.reshape(-1)

            # conserve memory
            del titles_dists, texts_dists, nouns_dists, adjs_dists,\
                titles_dense_dists, texts_dense_dists

            # estimate p(same_cluster)
            pairs_metrics[dep_var] = (
                cluster_classifier.predict_proba(pairs_metrics[features])[:, 1]
            )

            pairs_metrics = pairs_metrics[
                pairs_metrics[dep_var] > 0
            ]

            for x, y, p in zip(
                pairs_metrics['n_x'], pairs_metrics['n_y'],
                pairs_metrics[dep_var]
            ):
                same_cluster_probs[x, y] = p
                same_cluster_probs[y, x] = p

    return same_cluster_probs


# inspired by sklearn.cluster.AgglomerativeClustering
def agglomeratively_cluster_dist_matrix(X, C, linkage='average',
                                        max_merge_dist=None):
    """Agglomeratively clusters a distance matrix subject to connectivity.

    Given distance matrix X and connectivity matrix C, recursively merges pairs
        of samples and/or clusters such that the given linkage distance is
        minimized within clusters. Note that resulting clusters are fully
        connected per C (i.e., every sample in a cluster is connected to every
        other sample). If C is fully connected, a hierarchy of n_samples - 1
        clusters is returned. If not, or if max_cluster_dist is provided and
        surpassed, clustering stops early.

    Keyword arguments:
    X - distance matrix to cluster (n_samples x n_samples numpy array, can be
        sparse)
    C - connectivity matrix defining which samples can be clustered together
        (n_samples x n_samples sparse numpy array)
    linkage - either of 'average' or 'complete' (string, default: 'average')
    max_merge_dist - maximum allowable cluster merge distance (float, optional)

    Returns:
    list of strings - keywords
    """
    n_samples = X.shape[0]

    linkage_choices = {'complete': _hierarchical.max_merge,
                       'average': _hierarchical.average_merge}
    try:
        join_func = linkage_choices[linkage]
    except KeyError:
        raise ValueError(
            'Unknown linkage option, linkage should be one '
            'of %s, but %s was given' % (linkage_choices.keys(), linkage))

    # Remove diagonal from X and make sparse
    C = C.tocoo()
    diag_mask = (C.row != C.col)
    distances = X[C.row[diag_mask],
                  C.col[diag_mask]]
    distances = np.array(distances).reshape(-1, )
    X = C.copy()
    X.row = X.row[diag_mask]
    X.col = X.col[diag_mask]
    X.data = distances
    del diag_mask

    # prepare data structures
    max_nodes = 2 * n_samples - 1
    descendents = {sample: [sample] for sample in np.arange(n_samples)}
    node_n = np.zeros(max_nodes, dtype=np.intp)
    node_n[:n_samples] = 1
    parent = np.arange(max_nodes, dtype=np.intp)
    children = []
    distances = np.empty(max_nodes - n_samples)
    A = np.empty(max_nodes, dtype=object)
    inertia = list()

    # Transfer X to A, inertia
    # LIL seems to the best format to access the rows quickly,
    # without the numpy overhead of slicing CSR indices and data.
    X = X.tolil()
    # We are storing the graph in a list of IntFloatDict
    for r, (row, data) in enumerate(zip(X.rows, X.data)):
        A[r] = IntFloatDict(np.asarray(row, dtype=np.intp),
                            np.asarray(data, dtype=np.float64))
        # We keep only the upper triangular for the heap
        # Generator expressions are faster than arrays on the following
        for c, d in zip(row, data):
            if c < r:
                inertia.append(_hierarchical.WeightedEdge(d, r, c))
    del X

    heapify(inertia)
    C = C.tocsr()

    # recursive merge loop
    for k in range(n_samples, max_nodes):
        # identify the merge
        exit = True
        while True and inertia:
            edge = heappop(inertia)
            i = edge.a
            j = edge.b
            if node_n[i] and node_n[j]:  # are nodes active?
                union = descendents[i] + descendents[j]
                # are all descendents connected?
                if ((i < n_samples and j < n_samples)
                   or np.min(C[union, :][:, union])):
                    exit = False
                    break

        if exit:
            break

        # update data structures
        descendents[k] = descendents[i] + descendents[j]
        del descendents[i], descendents[j]
        n_i = node_n[i]
        n_j = node_n[j]
        node_n[k] = n_i + n_j
        node_n[i] = node_n[j] = 0
        dist = edge.weight
        if max_merge_dist and dist > max_merge_dist:
            break
        distances[k - n_samples] = dist
        parent[i] = parent[j] = k
        children.append((i, j))

        # update the structure matrix A and the inertia matrix
        # a clever 'min', or 'max' operation between A[i] and A[j]
        coord_col = join_func(A[i], A[j], node_n, n_i, n_j)
        for l, d in coord_col:
            A[l].append(k, d)
            # Here we use the information from coord_col (containing the
            # distances) to update the heap
            heappush(inertia, _hierarchical.WeightedEdge(d, k, l))
        A[k] = coord_col
        # Clear A[i] and A[j] to save memory
        A[i] = A[j] = 0

    # # return numpy array for efficient caching
    children = np.array(children)[:, ::-1]
    distances = distances[:len(children)].reshape(-1, 1)

    ac_result = np.hstack((children, distances))

    return ac_result


def gen_clusters(ac_result, n_clusters=None, max_dist=None,
                 return_descendents=False):
    """Generates cluster assignments given an agglomerative clustering run.

    Given the output of agglomeratively_cluster_dist_matrix and a number of
        clusters or a max dist, generates corresponding cluster assignments.

    Keyword arguments:
    ac_result -- the output from agglomeratively_cluster_dist_matrix (nx3 numpy
        array)
    n_clusters -- number of clusters into which to assign observations (
        integer, optional)
    max dist -- max dist for clusters to be merged (float, optional)
    return_descendents -- whether to return object cluster assignments (bool,
        default: False)

    Returns:
    numpy array -- cluster_labels
    dict -- cluster descendents in the form {cluster: [descendents]} (if
        return_descendents)
    """
    n_obs = len(ac_result) + 1
    descendents = {obs: [obs] for obs in np.arange(n_obs)}
    for n, clustering in enumerate(ac_result):
        dist = clustering[2]
        if ((max_dist and dist > max_dist)
                or (n_clusters and len(descendents) == n_clusters)):
            break
        cluster_id = n + n_obs
        lc = clustering[0]
        rc = clustering[1]
        descendents[cluster_id] = descendents[lc] + descendents[rc]
        del descendents[lc]
        del descendents[rc]
    cluster_labels = np.arange(0, n_obs)
    for cluster_id, obs_ids in descendents.items():
        for obs_id in obs_ids:
            cluster_labels[obs_id] = cluster_id

    if return_descendents:
        return cluster_labels, descendents
    return cluster_labels


def form_subtopic_clusters(df, matrices, batch_timestamp, batch_type,
                           return_df=False, write_to_mdb=True,
                           verbose=False):
    """Clusters articles at the subtopic level, then writes to MongoDB.

    Given a df of articles and associated matrices, forms a graph representing
        the probability that each article is in each other article's subtopic
        cluster. Writes topic and subtopic clusters to MongoDB after subtopic
        clusters are made.

    Keyword arguments:
    df -- dataset of articles to be clustered (pandas DataFrame)
    matrices -- matrices corresponding to articles from content_corpus (dict of
        numpy arrays)
    batch_timestamp -- timestamp that df clustering is initiated, to be passed
        on (datetime)
    batch_type -- type of df being clustered, to be passed on (string)
    return_df -- whether to return df with cluster labels added (bool)
    write_to_mdb -- whether to write formed subtopic clusters to MongoDB

    Returns:
    pandas DataFrame (if return_df)
    """
    n_articles = len(df)

    topic_categorization = df['topic_categorization_predicted'][0]
    region_categorization = df['region_categorization_predicted'][0]

    df['crawled_timestamp_num'] = df['crawled_timestamp'].apply(
        lambda x: x.timestamp()
    )

    topic_cluster_id = sha1(np.asarray(df['_id'])).hexdigest()

    outstr = '\n\nBeginning subtopic clustering...'
    outstr += ('\nTopic Cluster ID: %s' % str(topic_cluster_id))

    X = -get_same_cluster_probs(  # negative to support agglomerative cluster
        'subtopic',
        df,
        matrices['titles_tfidf_matrix'],
        matrices['texts_tfidf_matrix'],
        matrices['texts_nouns_tfidf_matrix'],
        matrices['texts_adjs_tfidf_matrix'],
        matrices['titles_dense_matrix'],
        matrices['texts_dense_matrix']
    ).tocsr()
    C = csr_matrix(X < 0, dtype='bool')

    # TODO: may need p_same_cluster_threshold to vary by topic, region category
    p_same_cluster_threshold = configs.SUBTOPIC_MIN_P_SAME_CLUSTER

    try:
        ac_result = agglomeratively_cluster_dist_matrix(
            X, C, max_merge_dist=-p_same_cluster_threshold
        )

        to_append = np.array(
            [[1e6, 1e6, 0]
             for i in range(n_articles - 1 - ac_result.shape[0])]
        )
        if to_append.size > 0:
            ac_result = np.vstack((ac_result, to_append))
        ac_result_dec = ac_result[::-1]

        optimal_n_clusters = (np.where(
            (-ac_result_dec[:, 2]) > p_same_cluster_threshold
        )[0][0] + 1)
    except:
        ac_result = agglomeratively_cluster_dist_matrix(X, C)

        to_append = np.array(
            [[1e6, 1e6, 0]
             for i in range(n_articles - 1 - ac_result.shape[0])]
        )
        if to_append.size > 0:
            ac_result = np.vstack((ac_result, to_append))
        ac_result_dec = ac_result[::-1]

        optimal_n_clusters = n_articles // 10

    outstr += ('\nOptimal N Clusters: %s' % (optimal_n_clusters))

    df['subtopic_cluster_id'] = gen_clusters(ac_result, optimal_n_clusters)

    if write_to_mdb:
        cluster_id_counts = df['subtopic_cluster_id'].value_counts()
        misc_cluster_ids = []
        for cluster_id, n_articles in zip(cluster_id_counts.index,
                                          cluster_id_counts.values):
            if n_articles >= configs.MIN_ARTICLES_IN_SUBTOPIC_CLUSTER:
                subset = df[df['subtopic_cluster_id'] == cluster_id]
                subset.sort_values(
                    by='quality_score', inplace=True, ascending=False
                )
                avg_crawl_time = np.mean(subset['crawled_timestamp_num'])
                min_crawl_time = np.min(subset['crawled_timestamp_num'])
                max_crawl_time = np.max(subset['crawled_timestamp_num'])
                subtopic_cluster_id = sha1(
                    np.asarray(subset['_id'])
                ).hexdigest()
                member_ids = subset['_id'].apply(
                    lambda x: str(x)
                ).str.cat(sep=', ')
                cluster_data = {
                    'batch_timestamp': batch_timestamp,
                    'batch_type': batch_type,
                    'create_timestamp': datetime.utcnow(),
                    'topic_cluster_id': topic_cluster_id,
                    'topic_categorization': topic_categorization,
                    'region_categorization': region_categorization,
                    'avg_crawl_time': avg_crawl_time,
                    'min_crawl_time': min_crawl_time,
                    'max_crawl_time': max_crawl_time,
                    'subtopic_cluster_id': subtopic_cluster_id,
                    'is_misc': False,
                    'n_articles': int(n_articles),
                    'member_ids': member_ids
                }
                CONTENT_CLUSTERS.insert_one(cluster_data)
            else:
                misc_cluster_ids += [cluster_id]
        if misc_cluster_ids:
            filt = df['subtopic_cluster_id'].apply(lambda x:
                                                   x in misc_cluster_ids)
            subset = df[filt]
            n_articles = len(subset)
            avg_crawl_time = np.mean(subset['crawled_timestamp_num'])
            min_crawl_time = np.min(subset['crawled_timestamp_num'])
            max_crawl_time = np.max(subset['crawled_timestamp_num'])
            subtopic_cluster_id = sha1(np.asarray(subset['_id'])).hexdigest()
            member_ids = subset['_id'].apply(lambda x:
                                             str(x)).str.cat(sep=', ')
            cluster_data = {
                'batch_timestamp': batch_timestamp,
                'batch_type': batch_type,
                'create_timestamp': datetime.utcnow(),
                'topic_cluster_id': topic_cluster_id,
                'topic_categorization': topic_categorization,
                'region_categorization': region_categorization,
                'avg_crawl_time': avg_crawl_time,
                'min_crawl_time': min_crawl_time,
                'max_crawl_time': max_crawl_time,
                'subtopic_cluster_id': subtopic_cluster_id,
                'is_misc': True,
                'n_articles': int(n_articles),
                'member_ids': member_ids
            }
            CONTENT_CLUSTERS.insert_one(cluster_data)

    if verbose:
        print(outstr)
    logging.info(outstr)

    if return_df:
        return df
