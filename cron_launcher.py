import sys

from content_corpus import enqueue_content_for_clustering

if __name__ == "__main__":
    days_of_recent_content = float(sys.argv[1])
    if len(sys.argv) > 2:  # second arg is city id
        city_id = int(sys.argv[2])
    else:
        city_id = None

    # days_of_recent_content is whole number, represent as int so trailing
    # zero is dropped in batch type
    if days_of_recent_content == round(days_of_recent_content):
        days_of_recent_content = int(days_of_recent_content)

    enqueue_content_for_clustering(
        days_of_recent_content=days_of_recent_content,
        city_id=city_id
    )
